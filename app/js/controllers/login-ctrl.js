/**
 * Created by sanjay on 3/25/15.
 */
App.controller('LoginController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, $timeout) {
    //initially set those objects to null to avoid undefined error
    // place the message if something goes wrong
    $scope.account = {};
    $scope.authMsg = '';

    $scope.loginAdmin = function () {
        $scope.authMsg = '';
        // var formdata = new FormData();
        // formdata.append('email', $scope.account.email)
        // formdata.append('password', $scope.account.password)

        $http({
            url: MY_CONSTANT.url + '/api/admin/login',
            method: "POST",
            data: {
                email : $scope.account.email,
                password : $scope.account.password
            }
        })
            .then(
            function (data) {

                console.log(data)

                if (data.status == 200) {
                    var someSessionObj = {'accesstoken': data.data.data.access_token};
                    $cookieStore.put('obj', someSessionObj);
                    console.log(someSessionObj)
                    
                    $state.go('app.dashboard');
                } else {
                    $scope.authMsg = data.message;

                }
            });
    };

    $scope.recover = function () {

        $http({
            url: MY_CONSTANT.url + '/api/admin/getPasswordToken',
            method: "PUT",
            data: {
                email: $scope.account.email
            }
        }).then(
            function (data) {

                console.log(data);
                if (data.status == 200) {
                    console.log(data)
                    $scope.token = data.data.data.password_reset_token.toString();
                    $cookieStore.put('passwordToken', $scope.token)

                        $scope.successMsg = data.data.message;
                    $timeout(function () {
                        $state.go('page.reset-password')
                    }, 3000);


                } else {
                    $scope.errorMsg = data.data.message;

                }
            })
    };

    $scope.resetPassword = function () {

        $http({
            url: MY_CONSTANT.url + '/api/admin/resetPassword',
            method: "PUT",
            params: {
                email: $scope.account.email,
                passwordResetToken: $cookieStore.get('passwordToken'),
                newPassword: $scope.account.password1
            }
        }).then(
            function (data) {

                console.log(data);

                if (data.status == 200) {
                    console.log(data)
                    $scope.successMsg = data.data.message;
                    $timeout(function () {
                        $state.go('page.login')
                    }, 3000);
                } else {
                    $scope.errorMsg = data.data.message;

                }
            })
    }

    $scope.logout = function () {
        $cookieStore.remove('obj');
        $state.go('page.login');
    }
});

