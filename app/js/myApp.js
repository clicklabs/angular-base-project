// To run this code, edit file
// index.html or index.jade and change
// html data-ng-app attribute from
// angle to myAppName
// -----------------------------------

var myApp = angular.module('myAppName', ['angle','uiGmapgoogle-maps']);

myApp.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
    GoogleMapApi.configure({
//    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}]);
myApp.run(["$log", function ($log) {

    $log.log('I\'m a line from custom.js');

}]);

App.constant("MY_CONSTANT", {
    "url": " http://52.90.184.241:3000"
});

App.constant("responseCode", {
    "SUCCESS": 200
});
myApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper) {
        'use strict';

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);

        // default route
        $urlRouterProvider.otherwise('/page/login');

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
            //
            // Single Page Routes
            // -----------------------------------
            .state('page', {
                url: '/page',
                templateUrl: 'app/pages/page.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley'),
                controller: ["$rootScope", function ($rootScope) {
                    $rootScope.app.layout.isBoxed = false;
                }]
            })
            .state('page.login', {
                url: '/login',
                title: "Login",
                templateUrl: 'app/pages/login.html'
            })
            .state('page.register', {
                url: '/register',
                title: "Register",
                templateUrl: 'app/pages/register.html'
            })
            .state('page.recover', {
                url: '/recover',
                title: "Recover",
                templateUrl: 'app/pages/recover.html'
            })

            .state('page.reset-password', {
                url: '/reset',
                title: "Reset Password",
                templateUrl: 'app/pages/reset-password.html'
            })
            .state('page.terms', {
                url: '/terms',
                title: "Terms & Conditions",
                templateUrl: 'app/pages/terms.html'
            })
            .state('page.404', {
                url: '/404',
                title: "Not Found",
                templateUrl: 'app/pages/404.html'
            })

            //App routes
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: helper.basepath('app.html'),
                controller: 'AppController',
                resolve: helper.resolveFor('modernizr', 'icons', 'screenfull')
            })
            .state('app.dashboard', {
                url: '/dashboard',
                title: 'Dashboard',
                templateUrl: helper.basepath('dashboard.html')
            })
            .state('app.listview', {
                url: '/listview',
                title: 'Data Table',
                templateUrl: helper.basepath('listview.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.pendingdrivers', {
                url: '/pendingdrivers',
                title: 'Pending Drivers',
                templateUrl: helper.basepath('pending-drivers.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','ngDialog')
            })
            .state('app.mapview', {
                url: '/mapview',
                title: 'Google Map',
                templateUrl: helper.basepath('mapview.html'),
                resolve: helper.resolveFor('loadGoogleMapsJS', function() { return loadGoogleMaps(); }, 'google-map')
            })
            .state('app.chart', {
                url: '/chart',
                title: 'Flot Chart',
                templateUrl: helper.basepath('chart.html'),
                resolve: helper.resolveFor('flot-chart','flot-chart-plugins')
            })
            .state('app.form', {
                url: '/form',
                title: 'Simple Form',
                templateUrl: helper.basepath('form.html'),
                resolve: helper.resolveFor('parsley')
            })
            .state('app.area-codes', {
                url: '/area-codes',
                title: 'Area Codes',
                templateUrl: helper.basepath('area-codes.html')
            })
            .state('app.add-kitchen', {
                url: '/add-kitchen',
                title: 'Add Kitchen',
                templateUrl: helper.basepath('add-kitchen.html')
            })
            .state('app.dish-categories', {
                url: '/dish-categories',
                title: 'Dish Categories',
                templateUrl: helper.basepath('dish-categories.html')
            })

            //
            // CUSTOM RESOLVES
            //   Add your own resolves properties
            //   following this object extend
            //   method
            // -----------------------------------
            // .state('app.someroute', {
            //   url: '/some_url',
            //   templateUrl: 'path_to_template.html',
            //   controller: 'someController',
            //   resolve: angular.extend(
            //     helper.resolveFor(), {
            //     // YOUR RESOLVES GO HERE
            //     }
            //   )
            // })
        ;


    }]);
